terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
#####
provider "aws" {
  region = var.region

}



#module "s3_bucket" {
#  source = "terraform-aws-modules/s3-bucket/aws"
#  version = "2.15.0"
#  bucket = "my-s3-bucket-raymond"
#  acl    = "private"

#  versioning = {
#    enabled = true
#  }

#}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
resource "aws_eip" "foo" {
  instance = aws_instance.foo.id
  vpc      = true
}

resource "aws_eip_association" "foo" {
  instance_id   = aws_instance.foo.id
  allocation_id = aws_eip.foo.id
}


resource "aws_instance" "foo" {
  ami           = data.aws_ami.ubuntu.id # ap-southeast-1
  instance_type = var.instance_type
  key_name      = var.ssh_key
  user_data = templatefile("${path.module}/files/startup.tpl", {
        timezone = var.timezone
        hostname = var.hostname
        new_user = var.new_user

  })
  


  tags = {
    Name = var.instance_tags["name"]
    User = var.instance_tags["user"]
    Purpose = var.instance_tags["purpose"]
    test = "dev2"

    }
  
  subnet_id = var.subnet_id
  

  credit_specification {
    cpu_credits = "unlimited"
  }
}


